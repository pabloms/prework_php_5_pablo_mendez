<?php

$original = fopen("documento.doc", 'w+');
$hoy = getdate();
fwrite($original, "$hoy[year]-$hoy[mon]-$hoy[wday] $hoy[hours]:$hoy[minutes]");
fclose($original);

$modificado = fopen("documento.doc.modificado", 'w+');
fwrite($modificado, "$hoy[year]-$hoy[mon]-$hoy[wday] $hoy[hours]:$hoy[minutes]");
fclose($modificado);